module bitbucket.org/pcas/sslflag

go 1.13

require (
	bitbucket.org/pcastools/flag v0.0.19
	github.com/mitchellh/go-homedir v1.1.0
)
