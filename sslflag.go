// SSLflag provides a standardised way to obtain SSL certificates and keys.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package sslflag

import (
	"crypto/x509"
	"encoding/pem"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"

	"bitbucket.org/pcastools/flag"
	"github.com/mitchellh/go-homedir"
)

// ClientSet names
const (
	clientSetName    = "SSL options"   // The set name
	disabledFlagName = "ssl-disabled"  // The flag name to disable SSL connections to servers
	certFlagName     = "ssl-cert-file" // The flag name for the certificate file
	certEnvName      = "PCAS_SSL_CERT" // The env var name for the certificate
)

// ServerSet names
const (
	serverSetName          = "Server SSL options"   // The set name
	serverDisabledFlagName = "server-ssl-disabled"  // The flag name to disable SSL connections from clients
	serverKeyFlagName      = "server-ssl-key-file"  // The flag name for the server's (private) key file
	serverCertFlagName     = "server-ssl-cert-file" // The flag name for the server's (public) certificate file
	serverKeyEnvName       = "PCAS_SERVER_SSL_KEY"  // The env var name for the server's (private) key
	serverCertEnvName      = "PCAS_SERVER_SSL_CERT" // The env var name for the server's (public) certificate
)

// disabledFlag is the command-line flag for disabling outgoing SSL connections.
type disabledFlag bool

// certFlag is the standard command-line flag for obtaining SSL certificates.
type certFlag struct {
	path      string // The path to the certificate file
	hasParsed bool   // Have we parsed?
	cert      []byte // The certificate
	err       error  // The parse error, if any
}

// ClientSet is the standard command-line set for client SSL options.
type ClientSet struct {
	disabled disabledFlag // The disabled flag
	cert     certFlag     // The certificate flag
}

// serverDisabledFlag is the command-line flag for disabling client (incoming) SSL connections.
type serverDisabledFlag bool

// keyFlag is the command-line flag for the SSL private key.
type keyFlag struct {
	path      string // The path to the key file
	hasParsed bool   // Have we parsed?
	key       []byte // The key
	err       error  // The parse error, if any
}

// keyCertFlag is the command-line flag for the SSL private key's certificate.
type keyCertFlag struct {
	path      string // The path to the certificate file
	hasParsed bool   // Have we parsed?
	cert      []byte // The certificate
	err       error  // The parse error, if any
}

// ServerSet is the standard command-line set for server SSL options.
type ServerSet struct {
	disabled serverDisabledFlag // The disabled flag
	key      keyFlag            // The key flag
	cert     keyCertFlag        // The key's certificate flag
}

// pcaAuthorityCertificate is the (public) authority certificate used to sign PCAS keys.
const pcaAuthorityCertificate = "-----BEGIN CERTIFICATE-----\nMIIG2DCCBMCgAwIBAgIVAPAatPA6gLeWb4FkQe8ynH24MPx5MA0GCSqGSIb3DQEB\nCwUAMIHzMQswCQYDVQQGEwJHQjEXMBUGA1UECBMOR3JlYXRlciBMb25kb24xGTAX\nBgNVBAcTEFNvdXRoIEtlbnNpbmd0b24xGDAWBgNVBAkTD0V4aGliaXRpb24gUm9h\nZDEQMA4GA1UEERMHU1c3IDJBWjE1MDMGA1UEChMsUGFyYWxsZWwgQ29tcHV0YXRp\nb25hbCBBbGdlYnJhIFN5c3RlbSAoUENBUykxIjAgBgNVBAsTGURlcGFydG1lbnQg\nb2YgTWF0aGVtYXRpY3MxKTAnBgNVBAMTIDA1YWE1NzQ4YjY3YTQ3MDRmYTAyOGQw\nMjQ5ZGUyZDFkMB4XDTIyMDIyMzIwNDIwNVoXDTMyMDIyMzIwNDIwNVowgfMxCzAJ\nBgNVBAYTAkdCMRcwFQYDVQQIEw5HcmVhdGVyIExvbmRvbjEZMBcGA1UEBxMQU291\ndGggS2Vuc2luZ3RvbjEYMBYGA1UECRMPRXhoaWJpdGlvbiBSb2FkMRAwDgYDVQQR\nEwdTVzcgMkFaMTUwMwYDVQQKEyxQYXJhbGxlbCBDb21wdXRhdGlvbmFsIEFsZ2Vi\ncmEgU3lzdGVtIChQQ0FTKTEiMCAGA1UECxMZRGVwYXJ0bWVudCBvZiBNYXRoZW1h\ndGljczEpMCcGA1UEAxMgMDVhYTU3NDhiNjdhNDcwNGZhMDI4ZDAyNDlkZTJkMWQw\nggIiMA0GCSqGSIb3DQEBAQUAA4ICDwAwggIKAoICAQDNoBZGl+BUzgxHfkmjFgtN\nOikx1A5qHRNwvo1fULgggEjnWvUNVXtBprDOyhEjawCC09jYCoevXLhOugFvb5Rq\nqJ5OGrLYLOdkEYLxiHGbv5v2vboLNw0hBrpzLDSTPeeL0K+7Op7EnmUMnD2CACxF\ngnxMqvxfBQeCTAWcq3Fzxpfxtc5Svl3E4LdTDbSE26tqn2yMBB60RGPxHjtjJ0oU\n18htOTszANlz10GErJjboEKWgtjApOmKx6YFApYL7PelWCV3hBA9l4FY52+xPcBT\na6KLA1kVFaV3Dlt8i5L23UKYRrnolCJI746DTdFCGaKhy8FJwJXEv6fl0abM7WaO\nQMAnuPx9O+wFo94jmVVlGu1TNVKbcBlnkskVA1e8sfKMMIY82DpfHKh+Po1MSo7D\nP/y9437cQQUJd23Pkv7oMoAUX+427/PMFjj3aL9CAYrRTQg6182CcvqVKsWVgHLH\nWdGXlt8NlOlJGvSHmLS4zsRZCti0P2OWHhkJ9Q0+wifKV6E+0I2A1u73J1t6Frnw\n+ENHgNRWjxJCtxZyi7hYBCjZV9AVgHoR1mbHKzCtEWay7piwGpp0KneT4au5EmDs\n1TD054Ii+66jlE8UkbuqpR2NU86RaDVdmmG++z8FJdZTJdj0fkp4IU/TI9QbRKD/\nthwevxKFT5xeyysUZGF0YQIDAQABo2EwXzAOBgNVHQ8BAf8EBAMCAoQwHQYDVR0l\nBBYwFAYIKwYBBQUHAwIGCCsGAQUFBwMBMA8GA1UdEwEB/wQFMAMBAf8wHQYDVR0O\nBBYEFJWaiE6g+vsSAPIqJqaOzpC6Bo25MA0GCSqGSIb3DQEBCwUAA4ICAQBhmK3M\nZUFMHeqBCn71X30Us83llPArZhbiMCibke1z8EH6fGIuWwNIJrrbfMFb4eMjn2KZ\nZBE1WGfGALAk46rsnUKbA12c+r9+ecpjKQN+D3jgHOeeBrjOjoAzNTSL/y63/+Mw\n0hwp5O7OMKI3PSiQKVFPYZ/UZCNhsGAz0Ym55fxFb7JoM6su+dO4ADcgleue9zYI\nF2/lMvccribTM/Ui13i7IX9prD1v05ZRJpKNf/TACQ9+kjc6OQ9qFbk1ZeT6Lkpu\ngx5OyoKlx/gDwWzy03On2Pswue1kDAF6EQY4mwf03eiILZiSnjPpK2yv6FZy5qN2\nv3DmNixJnH8Gy2tx89AbcPhdwK82mJG4infLMGRxU8CuoYNuSKEDg3coDbJDJ7EF\nYfkrqJbmLjuTSbu5za5PN+wrsKq6L0OkXt/X+BTMjJO9Po4wmR7TOXCry/6VP0Pj\niwKy9GDO/glnP4xP9w+MeIu0QX2itZO104JlnTsSLupfUxfzRQikk4ZMZFLIEEDJ\nliHe5KoLylxkZCjIQhONB6mCRxd4UAhhB53DlkjnLHmtSkioTKztGvKgiuaTcdBw\nAMG48cvHSO7UHdB4D+4f2zp9pn4zQhty986LSoNGElbDhvVvkFgmEIHc6HNYt6ay\nrCKM4vC7GGOBs6GS7UVY5pLoyvus6W8PLChaQQ==\n-----END CERTIFICATE-----"

// certificates can be set at compile to inject custom certificates.
var certificates = ""

// errFlagIsNil indicates that the flag is nil.
var errFlagIsNil = errors.New("flag is nil")

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init validates any custom certificates.
func init() {
	if len(certificates) != 0 {
		// Tidy up the certificate before validating it
		certificates = strings.ReplaceAll(certificates, "\\n", "\n")
		certificates = strings.ReplaceAll(certificates, "_", " ")
		certificates = pcaAuthorityCertificate + "\n" + certificates
	} else {
		certificates = pcaAuthorityCertificate
	}
	// Validate the certificate
	err := validateSSLCert([]byte(certificates))
	if err != nil {
		panic(fmt.Sprintf("malformed certificates: %s\n", err))
	}
}

// validateSSLCert validates the given certificate.
func validateSSLCert(cert []byte) error {
	for len(cert) > 0 {
		p, rest := pem.Decode(cert)
		if p == nil {
			return nil
		} else if p.Type == "CERTIFICATE" && len(p.Headers) == 0 {
			if _, err := x509.ParseCertificate(p.Bytes); err != nil {
				return err
			}
		}
		// Move on
		cert = rest
	}
	return nil
}

// appendCert appends 'val' to 'cert'.
func appendCert(cert []byte, val string) []byte {
	if n := len(cert); n != 0 && cert[n-1] != '\n' {
		cert = append(cert, '\n')
	}
	return append(cert, []byte(val)...)
}

// parseSSLCert loads the SSL certificates from the file with given path (if the path is non-empty). If the environment variable certEnv is set, the SSL certificates in certEnv will *also* be included. Any compile-time certificates stored in 'certificates' will *also* be included. The certificates will be validated before returning.
func parseSSLCert(path string, certEnv string) ([]byte, error) {
	var cert []byte
	// Add any compile-time certificates
	if len(certificates) != 0 {
		cert = appendCert(cert, certificates)
	}
	// Load the file
	if len(path) != 0 {
		val, err := ioutil.ReadFile(path)
		if err != nil {
			return nil, fmt.Errorf("unable to read SSL certificate file: %w", err)
		}
		cert = appendCert(cert, string(val))
	}
	// Load the environment variable
	if len(certEnv) != 0 {
		if val := os.Getenv(certEnv); len(val) != 0 {
			cert = appendCert(cert, val)
		}
	}
	// Validate the certificates
	if err := validateSSLCert(cert); err != nil {
		return nil, fmt.Errorf("SSL certificate validation failed: %w", err)
	}
	return cert, nil
}

// certStatusString returns a status string for any certificate stored in the indicated environment variable.
func certStatusString(certEnv string) string {
	status := "set"
	if val, ok := os.LookupEnv(certEnv); !ok {
		status = "unset"
	} else if len(val) == 0 {
		status = "set, but empty"
	} else if validateSSLCert([]byte(val)) != nil {
		status = "set, but invalid"
	}
	return status
}

// keyStatusString returns a status string for any key stored in the indicated environment variable.
func keyStatusString(keyEnv string) string {
	status := "set"
	if val, ok := os.LookupEnv(keyEnv); !ok {
		status = "unset"
	} else if len(val) == 0 {
		status = "set, but empty"
	}
	return status
}

// readFileOrEnv returns the contents of the file at the given path or, if the path is empty, the contents of the indicated environment variable.
func readFileOrEnv(path string, env string) ([]byte, error) {
	// If the path is non-empty, we return the contents of the file...
	if len(path) != 0 {
		val, err := ioutil.ReadFile(path)
		if err != nil {
			return nil, fmt.Errorf("unable to read file: %w", err)
		}
		return val, nil
	}
	// ...otherwise we return the value of the environment variable
	if len(env) != 0 {
		return []byte(os.Getenv(env)), nil
	}
	return nil, nil
}

// readPrivateKeyCertificate reads in a private key certificate from the given path, if set, otherwise from the environment variable. The certificate will be validated before returning.
func readPrivateKeyCertificate(path string, env string) ([]byte, error) {
	// Load and validate the certificate
	cert, err := readFileOrEnv(path, env)
	if err != nil {
		return nil, err
	} else if len(cert) == 0 {
		return nil, errors.New("an SSL private key certificate must be provided")
	} else if err = validateSSLCert(cert); err != nil {
		return nil, fmt.Errorf("SSL private key certificate validation failed: %w", err)
	}
	// Return success
	return cert, err
}

// readPrivateKey reads in a private key from the given path, if set, otherwise from the environment variable.
func readPrivateKey(path string, env string) ([]byte, error) {
	// Load the key
	key, err := readFileOrEnv(path, env)
	if err != nil {
		return nil, err
	} else if len(key) == 0 {
		return nil, errors.New("an SSL private key must be provided")
	}
	// Return success
	return key, err
}

/////////////////////////////////////////////////////////////////////////
// disabledFlag functions
/////////////////////////////////////////////////////////////////////////

// Name returns the flag name (without leading hyphen).
func (*disabledFlag) Name() string {
	return disabledFlagName
}

// Description returns a one-line description of this variable.
func (f *disabledFlag) Description() string {
	return fmt.Sprintf("Disable SSL encryption (default: %v)", *f)
}

// Usage returns long-form usage text.
func (f *disabledFlag) Usage() string {
	return fmt.Sprintf("If -%s is true, outgoing connections will not be encrypted.", f.Name())
}

// Parse parses the string.
func (f *disabledFlag) Parse(in string) error {
	if f == nil {
		return errFlagIsNil
	}
	val, err := strconv.ParseBool(strings.TrimSpace(in))
	if err != nil {
		return err
	}
	*f = disabledFlag(val)
	return nil
}

// IsBoolean indicates that this flag is a boolean flag in the sense of the flag package.
func (*disabledFlag) IsBoolean() bool {
	return true
}

/////////////////////////////////////////////////////////////////////////
// certFlag functions
/////////////////////////////////////////////////////////////////////////

// Name returns the flag name (without leading hyphen).
func (*certFlag) Name() string {
	return certFlagName
}

// EnvName returns the name of the environment variable associated with this flag.
func (*certFlag) EnvName() string {
	return certEnvName
}

// Description returns a one-line description of this variable.
func (*certFlag) Description() string {
	return "The path to the SSL certificate file"
}

// Usage returns long-form usage text.
func (f *certFlag) Usage() string {
	status := certStatusString(f.EnvName())
	return fmt.Sprintf("The flag -%s can be used to specify the path to a file from which to read SSL certificates. SSL certificates will also be read from the environment variable %s (currently %s).", f.Name(), f.EnvName(), status)
}

// Parse parses the string.
func (f *certFlag) Parse(in string) error {
	if f == nil {
		return errFlagIsNil
	}
	s, err := homedir.Expand(in)
	if err != nil {
		return err
	}
	f.path = s
	return nil
}

// Certificate returns any SSL certificates. This should only be called after the flags have been parsed.
func (f *certFlag) Certificate() ([]byte, error) {
	// Sanity check
	if f == nil {
		return nil, errFlagIsNil
	}
	// Do we already know the certificate?
	if !f.hasParsed {
		f.hasParsed = true
		f.cert, f.err = parseSSLCert(f.path, f.EnvName())
	}
	// Return any parse error
	if f.err != nil {
		return nil, f.err
	}
	// Make a copy of the certificate and return
	cert := make([]byte, len(f.cert))
	copy(cert, f.cert)
	return cert, nil
}

/////////////////////////////////////////////////////////////////////////
// ClientSet functions
/////////////////////////////////////////////////////////////////////////

// Flags returns the members of the set.
func (s *ClientSet) Flags() []flag.Flag {
	if s == nil {
		return nil
	}
	return []flag.Flag{&s.disabled, &s.cert}
}

// Name returns the name of this collection of flags.
func (*ClientSet) Name() string {
	return clientSetName
}

// UsageFooter returns the footer for the usage message for this flag set.
func (*ClientSet) UsageFooter() string {
	return ""
}

// UsageHeader returns the header for the usage message for this flag set.
func (*ClientSet) UsageHeader() string {
	return ""
}

// Validate validates the flag set.
func (s *ClientSet) Validate() error {
	if s == nil {
		return errFlagIsNil
	}
	_, err := s.cert.Certificate()
	return err
}

// Disabled returns true iff SSL is disabled. This should only be called after a successful call to Validate.
func (s *ClientSet) Disabled() bool {
	if s == nil {
		return true
	}
	return bool(s.disabled)
}

// Certificate returns any SSL certificates. This should only be called after a successful call to Validate.
func (s *ClientSet) Certificate() []byte {
	if s.Disabled() {
		return nil
	}
	cert, err := s.cert.Certificate()
	if err != nil {
		panic(err) // The call to Validate would have failed
	}
	return cert
}

/////////////////////////////////////////////////////////////////////////
// serverDisabledFlag functions
/////////////////////////////////////////////////////////////////////////

// Name returns the flag name (without leading hyphen).
func (*serverDisabledFlag) Name() string {
	return serverDisabledFlagName
}

// Description returns a one-line description of this variable.
func (f *serverDisabledFlag) Description() string {
	return fmt.Sprintf("Disable server-side SSL encryption (default: %v)", *f)
}

// Usage returns long-form usage text.
func (f *serverDisabledFlag) Usage() string {
	return fmt.Sprintf("If -%s is true, client connections to the server will not be encrypted.", f.Name())
}

// Parse parses the string.
func (f *serverDisabledFlag) Parse(in string) error {
	if f == nil {
		return errFlagIsNil
	}
	val, err := strconv.ParseBool(strings.TrimSpace(in))
	if err != nil {
		return err
	}
	*f = serverDisabledFlag(val)
	return nil
}

// IsBoolean indicates that this flag is a boolean flag in the sense of the flag package.
func (*serverDisabledFlag) IsBoolean() bool {
	return true
}

/////////////////////////////////////////////////////////////////////////
// keyFlag functions
/////////////////////////////////////////////////////////////////////////

// Name returns the flag name (without leading hyphen).
func (*keyFlag) Name() string {
	return serverKeyFlagName
}

// EnvName returns the name of the environment variable associated with this flag.
func (*keyFlag) EnvName() string {
	return serverKeyEnvName
}

// Description returns a one-line description of this variable.
func (*keyFlag) Description() string {
	return "The path to the SSL private key file"
}

// Usage returns long-form usage text.
func (f *keyFlag) Usage() string {
	status := keyStatusString(f.EnvName())
	return fmt.Sprintf("The flag -%s can be used to specify the path to a file from which to read the SSL private key. If -%s is not set, the SSL private key will be read from the environment variable %s (currently %s).", f.Name(), f.Name(), f.EnvName(), status)
}

// Parse parses the string.
func (f *keyFlag) Parse(in string) error {
	if f == nil {
		return errFlagIsNil
	}
	s, err := homedir.Expand(in)
	if err != nil {
		return err
	}
	f.path = s
	return nil
}

// Key returns the SSL key. This should only be called after the flags have been parsed.
func (f *keyFlag) Key() ([]byte, error) {
	// Sanity check
	if f == nil {
		return nil, errFlagIsNil
	}
	// Do we already know the key?
	if !f.hasParsed {
		f.hasParsed = true
		f.key, f.err = readPrivateKey(f.path, f.EnvName())
	}
	// Return any parse error
	if f.err != nil {
		return nil, f.err
	}
	// Make a copy of the key and return
	key := make([]byte, len(f.key))
	copy(key, f.key)
	return key, nil
}

/////////////////////////////////////////////////////////////////////////
// keyCertFlag functions
/////////////////////////////////////////////////////////////////////////

// Name returns the flag name (without leading hyphen).
func (*keyCertFlag) Name() string {
	return serverCertFlagName
}

// EnvName returns the name of the environment variable associated with this flag.
func (*keyCertFlag) EnvName() string {
	return serverCertEnvName
}

// Description returns a one-line description of this variable.
func (*keyCertFlag) Description() string {
	return "The path to the SSL private key's certificate file"
}

// Usage returns long-form usage text.
func (f *keyCertFlag) Usage() string {
	status := certStatusString(f.EnvName())
	return fmt.Sprintf("The flag -%s can be used to specify the path to a file from which to read the SSL private key's certificate. If -%s is not set, the SSL private key's certificate will be read from the environment variable %s (currently %s).", f.Name(), f.Name(), f.EnvName(), status)
}

// Parse parses the string.
func (f *keyCertFlag) Parse(in string) error {
	if f == nil {
		return errFlagIsNil
	}
	s, err := homedir.Expand(in)
	if err != nil {
		return err
	}
	f.path = s
	return nil
}

// Certificate returns the SSL certificate. This should only be called after the flags have been parsed.
func (f *keyCertFlag) Certificate() ([]byte, error) {
	// Sanity check
	if f == nil {
		return nil, errFlagIsNil
	}
	// Do we already know the certificate?
	if !f.hasParsed {
		f.hasParsed = true
		f.cert, f.err = readPrivateKeyCertificate(f.path, f.EnvName())
	}
	// Return any parse error
	if f.err != nil {
		return nil, f.err
	}
	// Make a copy of the certificate and return
	cert := make([]byte, len(f.cert))
	copy(cert, f.cert)
	return cert, nil
}

/////////////////////////////////////////////////////////////////////////
// ServerSet functions
/////////////////////////////////////////////////////////////////////////

// Flags returns the members of the set.
func (s *ServerSet) Flags() []flag.Flag {
	if s == nil {
		return nil
	}
	return []flag.Flag{&s.disabled, &s.key, &s.cert}
}

// Name returns the name of this collection of flags.
func (*ServerSet) Name() string {
	return serverSetName
}

// UsageFooter returns the footer for the usage message for this flag set.
func (s *ServerSet) UsageFooter() string {
	if s == nil {
		return ""
	}
	return fmt.Sprintf("The SSL private key and certificate will be ignored if -%s is true.", s.disabled.Name())
}

// UsageHeader returns the header for the usage message for this flag set.
func (*ServerSet) UsageHeader() string {
	return ""
}

// Validate validates the flag set.
func (s *ServerSet) Validate() error {
	// Sanity check
	if s == nil {
		return errFlagIsNil
	}
	// Are we allowing insecure connections? If so, there's nothing more to do.
	if s.disabled {
		return nil
	}
	// A key and certificate are required
	if _, err := s.key.Key(); err != nil {
		return err
	} else if _, err = s.cert.Certificate(); err != nil {
		return err
	}
	// FIXME: It would be good to verify that the key and certificate are
	// compatible, but I'm afraid I don't know how to do this.
	return nil
}

// Disabled returns true iff SSL is disabled. This should only be called after a successful call to Validate.
func (s *ServerSet) Disabled() bool {
	if s == nil {
		return true
	}
	return bool(s.disabled)
}

// Certificate returns the SSL certificate for the private key. This should only be called after a successful call to Validate.
func (s *ServerSet) Certificate() []byte {
	if s.Disabled() {
		return nil
	}
	cert, err := s.cert.Certificate()
	if err != nil {
		panic(err) // The call to Validate would have failed
	}
	return cert
}

// Key returns the SSL private key. This should only be called after a successful call to Validate.
func (s *ServerSet) Key() []byte {
	if s.Disabled() {
		return nil
	}
	key, err := s.key.Key()
	if err != nil {
		panic(err) // The call to Validate would have failed
	}
	return key
}
